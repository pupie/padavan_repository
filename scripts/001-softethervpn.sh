#!/bin/sh

mkdir -p /tmp/softethervpn

if [ ! -e "/tmp/softethervpn/vpn_server.config" ]
then 
	cp /etc/storage/softethervpn/vpn_server.config /tmp/softethervpn/vpn_server.config
fi


if [ -e "/tmp/softethervpn/vpnserver" ]
then
	rm /tmp/softethervpn/vpnserver
	wget https://coding.net/u/pupie/p/padavan_repository/git/raw/master/binaries/softethervpn/vpnserver -P /tmp/softethervpn/
fi


if [ -e "/tmp/softethervpn/hamcore.se2" ]
then
	rm /tmp/softethervpn/hamcore.se2
	wget https://coding.net/u/pupie/p/padavan_repository/git/raw/master/binaries/softethervpn/hamcore.se2 -P /tmp/softethervpn/
fi


chmod 777 /tmp/softethervpn/vpnserver
/tmp/softethervpn/vpnserver start




