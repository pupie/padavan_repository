#!/bin/sh

mkdir -p /tmp/frp

if [ ! -e "/tmp/frp/frpc" ]
	then wget https://gitee.com/pupie/padavan_repository/raw/master/binaries/frp/frpc -P /tmp/frp/
fi

chmod 777 /tmp/frp/frpc

/tmp/frp/frpc -c /etc/storage/frp/frpc.ini >/dev/null 2>&1 &
