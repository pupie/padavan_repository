#!/bin/sh

# #Enable Download Speed Limit
# iptables -I FORWARD 2 -m limit -d 192.168.1.50 --limit 8/s --limit-burst 8 -j ACCEPT 
# iptables -I FORWARD 3 -d 192.168.1.50 -j DROP

# #Enable Upload Speed Limit
# iptables -I FORWARD 4 -m limit -s 192.168.1.50 --limit 8/s --limit-burst 8 -j ACCEPT 
# iptables -I FORWARD 5 -s 192.168.1.50 -j DROP

# #Disable Download Speed Limit
# iptables -D FORWARD -m limit -d 192.168.1.50 --limit 8/s --limit-burst 8 -j ACCEPT 
# iptables -D FORWARD -d 192.168.1.50 -j DROP

# #Disable Upload Speed Limit
# iptables -D FORWARD -m limit -s 192.168.1.50 --limit 8/s --limit-burst 8 -j ACCEPT 
# iptables -D FORWARD -s 192.168.1.50 -j DROP

# 7/s:11k	35/s:50k	70/s:100k
#
#
# logger -t "【SpeedLimit】" "开始对$IP限速" 
#




IP=192.168.1.50
lmt=10


if [ "$1" == "enable" ]
	then 
		echo "Enable Speed Limit!"
		logger -t "【SpeedLimit】" "开始对$IP限速" 
		#Enable Download Speed Limit
		iptables -I FORWARD 2 -m limit -d $IP --limit $lmt/s --limit-burst 7 -j ACCEPT
		iptables -I FORWARD 3 -d $IP -j DROP
		#Enable Upload Speed Limit
		iptables -I FORWARD 4 -m limit -s $IP --limit $lmt/s --limit-burst 7 -j ACCEPT
		iptables -I FORWARD 5 -s $IP -j DROP


elif [ "$1" == "disable" ]
	then 
		echo "Disable Speed Limit!"
		logger -t "【SpeedLimit】" "停止对$IP限速" 
		#Disable Download Speed Limit
		iptables -D FORWARD -m limit -d $IP --limit $lmt/s --limit-burst 7 -j ACCEPT 
		iptables -D FORWARD -d $IP -j DROP

		#Disable Upload Speed Limit
		iptables -D FORWARD -m limit -s $IP --limit $lmt/s --limit-burst 7 -j ACCEPT 
		iptables -D FORWARD -s $IP -j DROP
else
	echo "Wrong parameter!"
fi

